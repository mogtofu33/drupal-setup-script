#!/bin/bash

PROGNAME=${0##*/} 
PROGVERSION=0.1

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
CONFIG_FILE="$SCRIPT_DIR/"".conf"

# Source helpers.
source $SCRIPT_DIR/helpers.sh
# Config file.
[[ -z $CONFIG_FILE ]] && die "Couldn't find a config file, are you within the correct path?"
source $CONFIG_FILE

# Environment values.
DRUSH=`which drush`
MYSQL=`which mysql`

print_help() {
cat <<-HELP
Drupal script installation
 Options with arguments:
  -d,  --dir          Drupal dir inside root, default "drupal"
  -url                Drupal url, default "drupal.vm"
  --root              Webserver root dir, default "/var/www/html"
  -db, --database     Database name, default "drupal_db"
  -dbu                Database user, default "root"
  -dbp                Database password, default "root"
  -lg, --locale       Set site lang code (en, fr, es...)
  --country           Set site country (EN, FR, ES...)
  -m, --mode          Setup modes:
                      min
                        Initialize a new project with minimal config
                      basic
                        Initialize a new project with basic config
                      full
                        Initialize a new project with full config
                      test
                        Initialize a new project with full config and test modules
 Options:
  -novhost            Do not try to set vhost file from model (see conf)
  -noremove           Do not remove dir and db name
  -nl, --nolocale     Install in english, ignore lang
  -i18n, --i18n       Download and install i18n related modules
  -q,  --quiet        Quiet (minimal output, messages are logged)
  -l,  --log          Log output to file drupal_deploy.log
  -h,  --help         Display this help and exit
  -V,  --version      Output version information and exit


Usage: (sudo) bash ${0##*/} -d=PATH -db=DB_NAME -mu=DB_USER -mp=DB_PASS -nl -q

Example: (sudo) bash ${0##*/} -d=/usr/local/apache2/htdocs -u=root -p=password

HELP
exit 0
}

# Default values.
nolocale=0
i18n=0
novhost=0
noremove=0

# Check command flags invoked and store possible arguments.
for i in "$@"
do
case $i in
    -h|--help) print_help >&2; safe_exit ;;
    -V|--version) out "$PROGNAME v$PROGVERSION"; safe_exit ;;
    -q|--quiet) quiet=1 ;;
    -nl|--nolocale) nolocale=1 ;;
    --novhost) novhost=1 ;;
    --noremove) noremove=1 ;;
    -i18n) i18n=1 ;;
    -lg=*|--locale=*) SITE_LANG="${i#*=}" ;;
    -ctr=*--country=*) SITE_COUNTRY="${i#*=}" ;;
    -m=*|--mode=*) MODE="${i#*=}" ;;
    -db=*|--database=*) DB_NAME="${i#*=}" ;;
    -dbu=*) DB_USER="${i#*=}" ;;
    -dbp=*) DB_PASS="${i#*=}" ;;
    -d=*|-dir=*) DIR="${i#*=}" ;;
    -url=*) URL="${i#*=}" ;;
    -root=*) ROOT="${i#*=}" ;;
esac
done

# Default language as english if no locale.
if [[ $nolocale == 1 ]]; then
 SITE_LANG="en"
 SITE_COUNTRY="US"
fi

# Check profile mode.
if [ "$MODE" != "min" ] && [ "$MODE" != "basic" ] && [ "$MODE" != "full" ] && [ "$MODE" != "test" ]; then
  MODE="basic"
fi

# Check profile mode.
if [ "$MODE" == "min" ]; then
  PROFILE="minimal"
else
  PROFILE="standard"
fi

# Settings.
SRC=$ROOT/$DIR

# Main Drush command builder.
# @TODO: handle error and warning from Drush.
drush_cmd() {
  CMD="$DRUSH -r $SRC -y $@"
  if [[ $quiet == 1 ]]; then
    SUFFIX="3>&1 1>>${LOG_FILE} 2>&1"
  else
    SUFFIX="2>&1"
  fi
  eval $CMD $SUFFIX
}

# Start information.
script_infos () {
  # Start deploy.
  echo -e "$BLUE1 Type --help or -h For help using this script.
$CYAN*** Script configuration **************************************************************
$CYAN MySQL:          $CYAN_BOLD $(mysql --version)
$CYAN PHP:            $CYAN_BOLD $(a=$(php --version);b=$(echo ${a%%\-*});echo $b)
$CYAN Drush:         $CYAN_BOLD $(drush --version)
$CYAN Bash:           $CYAN_BOLD $(a=$(bash --version);b=$(echo ${a%%\-*});echo $b)
$CYAN kernel:         $CYAN_BOLD $(uname -ri)
$CYAN Apache:         $CYAN_BOLD $(httpd -v | head -1)
$CYAN Install dir:    $YELLOW $SRC
$CYAN Website url:    $YELLOW http://$URL
$CYAN Website owner:  $YELLOW $OWNER:$GROUP
$CYAN MySQL settings: $YELLOW $DB_USER:$DB_PASS@$DB_HOST/$DB_NAME"
if [[ $quiet == 1 ]]; then
  echo -e "$CYAN Setup mode:     $YELLOW_BOLD $MODE$YELLOW with LOG to$YELLOW_BOLD ${LOG_FILE}"
else
  echo -e "$CYAN Setup mode:     $YELLOW_BOLD $MODE"
fi
echo -e "$CYAN Locale:         $YELLOW $SITE_LANG
$CYAN User 1 :        $YELLOW $USER | $PASS | $MAIL
$CYAN***************************************************************************************$ENDCOLOR"
	if ! confirm "Continue with these settings?" ]]; then
	  err "Operation aborted"
	  exit 0
	fi
}

# Remove folder and db, create db.
remove() {
  info "Remove dir and db, create db..."
  rm -rf $SRC
  if [ -d $MYSQL_DIR/$DB_NAME ] ; then
    if [ -z "$DB_PASS" ];then
      $MYSQL -h $DB_HOST -u $DB_USER -e "DROP DATABASE $DB_NAME;"
    else
      $MYSQL -h $DB_HOST -u $DB_USER -p$DB_PASS -e "DROP DATABASE $DB_NAME;"
    fi
  fi
  # Create db and grant privileges.
  if [ -z "$DB_PASS" ];then
    $MYSQL -h $DB_HOST -u $DB_USER -e "CREATE DATABASE $DB_NAME;"
    $MYSQL -h $DB_HOST -u $DB_USER -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER ON ${DB_NAME}.* TO '${DB_USER}'@'localhost';"
    $MYSQL -h $DB_HOST -u $DB_USER -e "FLUSH PRIVILEGES;"
  else
    $MYSQL -h $DB_HOST -u $DB_USER -p$DB_PASS -e "CREATE DATABASE $DB_NAME;"
    $MYSQL -h $DB_HOST -u $DB_USER -p$DB_PASS -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER ON ${DB_NAME}.* TO '${DB_USER}'@'localhost';"
    $MYSQL -h $DB_HOST -u $DB_USER -p$DB_PASS -e "FLUSH PRIVILEGES;"
  fi
  ok
}

# Set variables.
# Pass the array name as a string.
# TODO: Looking for a non-evil solution with an actual array reference.
# http://stackoverflow.com/questions/4069188/how-to-pass-an-associative-array-as-argument-to-a-function-in-bash
setVariables() {
  for arg in $*; do
    for var in $(eval echo \${!${arg}[@]}); do
        drush_cmd vset --exact ${var} $(eval echo \${${arg}["$var"]})
    done
  done
}

# Use `drush vget` and look through for what you find interesting.
declare -A VARIABLES=(
  ["configurable_timezones"]=0
  ["image_jpeg_quality"]="95"
  ["pathauto_case"]="1"
  ["pathauto_transliterate"]="1"
  ["pathauto_node_pattern"]="[node:title]"
  ["pathauto_punctuation_quotes"]="1"
  ["pathauto_punctuation_slash"]="2"
  ["pathauto_punctuation_hyphen"]="1"
  ["pathauto_user_pattern"]="profile/[user:name]"
  ["user_admin_role"]="3"
  ["user_register"]="0"
  ["user_pictures"]="0"
  ["user_default_timezone"]="0"
  ["user_email_verification"]="0"
  ["file_private_path"]="sites/default/files/private"
)
declare -A VARIABLES_LOCALE_FR=(
  ["date_first_day"]="1"
  ["date_api_use_iso8601"]="1"
  ["date_format_long"]="l, j F, Y - H:i"
  ["date_format_medium"]="D, d/m/Y - H:i"
  ["date_format_short"]="d/m/Y - H:i"
)
declare -A VARIABLES_DEVEL=(
  ["devel_memory"]="1"
  ["dev_timer"]="1"
  ["views_ui_always_live_preview"]="0"
  ["views_ui_show_advanced_column"]="1"
  ["views_ui_show_advanced_help_warning"]="0"
  ["views_ui_show_sql_query"]="1"
)
declare -A VARIABLES_FULL=(
  ["captcha_add_captcha_description"]="1"
  ["captcha_default_challenge"]="hidden_captcha/Hidden CAPTCHA"
  ["colorbox_inline"]="1"
)
declare -A VARIABLES_UNUSED=(
  ["html5_tools_override_views_field_rewrite_elements"]=1
  ["menu_block_suppress_core"]=1
  ["i18n_select_missing_translation"]=1
  ["i18n_hide_translation_links"]=1
  ["blocktheme_show_custom_block_theme"]=1
  ["blocktheme_themes"]="min|Minimalism"
  ["date_format_html5_tools_iso8601"]="c"
)

# Setup Drupal
drupal_install() {
  info "Downloading Drupal..."
  drush_cmd dl --destination=$ROOT --drupal-project-rename=$DIR
  ok
  info "Drupal installation..."
  # Drush site install command.
  drush_cmd site-install $PROFILE \
  --db-url=mysql://$DB_USER:$DB_PASS@$DB_HOST/$DB_NAME \
  --account-mail=$MAIL --account-name=$USER --account-pass=\"$PASS\" \
  --site-name=\"$SITE_NAME\" --site-mail=$MAIL \
  --locale=$SITE_LANG \
  install_configure_form.site_default_country=$SITE_COUNTRY
  ok
  info "Disable unused..."
  drush_cmd dis ${MODULES_DISABLE[@]}
  drush_cmd pm-uninstall ${MODULES_DISABLE[@]}
  ok
  info "Setup modules..."
  drush_cmd dl ${MODULES_CONTRIB[@]} ${MODULES_CONTRIB_DEV[@]}
  drush_cmd en ${MODULES_CONTRIB[@]} ${MODULES_CONTRIB_ENABLE[@]}
  if [[ $MODE == "full" ]]; then
    drush_cmd dl ${MODULES_CONTRIB_FULL[@]} ${MODULES_CONTRIB_FULL_DEV[@]} ${MODULES_CONTRIB_FULL_ONLY_DL[@]}
    drush_cmd en ${MODULES_CONTRIB_FULL[@]} ${MODULES_CONTRIB_FULL_ENABLE[@]}
  fi
  if [[ $MODE == "test" ]]; then
    drush_cmd dl ${MODULES_CONTRIB_TEST[@]}
    drush_cmd en ${MODULES_CONTRIB_TEST[@]} ${MODULES_CONTRIB_TEST_ENABLE[@]}
  fi
  if [[ i18n == 1 ]]; then
    drush_cmd dl ${MODULES_I18N[@]}
    drush_cmd en ${MODULES_I18N[@]}
  fi
  ok
  info  "Setup libraries..."
  mkdir -p $SRC/sites/all/libraries
  # Get ckeditor.
  if [[ $quiet == 1 ]]; then
    wget -q -P $SRC/sites/all/libraries/ $CKEDITOR 
    tar -xf $SRC/sites/all/libraries/ckeditor*.gz -C $SRC/sites/all/libraries/
  else
    wget -P $SRC/sites/all/libraries/ $CKEDITOR 
    `tar -xf $SRC/sites/all/libraries/ckeditor*.gz -C $SRC/sites/all/libraries/ >/dev/null 2>&1`
  fi
  rm -rf $SRC/sites/all/libraries/ckeditor*.*
  if [[ $MODE == "full" ]]; then
    # Colorbox.
    drush_cmd colorbox-plugin
  fi
  ok
  if [[ $SITE_LANG != 'en' ]]; then
    info "Locale setup..."
    drush_cmd dl drush_language
    drush_cmd language-add $SITE_LANG
    drush_cmd language-default $SITE_LANG
    drush_cmd l10n-update-refresh
    drush_cmd l10n-update
    ok
  fi
  info "Basic feature..."
  if [[ $MODE != "full" ]]; then
    FEATURE="basic_setup"
  fi
  if [[ $MODE == "full" ]]; then
    FEATURE="advanced_setup"
  fi
  cp -r $SCRIPT_DIR/features/$FEATURE $SRC/sites/all/modules/
  drush_cmd en $FEATURE
  ok
  info "Settings..."
  setVariables VARIABLES
  setVariables VARIABLES_DEVEL
  if [[ $SITE_LANG == 'fr' ]]; then
    setVariables VARIABLES_LOCALE_FR
  fi
  if [[ $MODE == "full" ]]; then
    setVariables VARIABLES_FULL
  fi
  # Missing folders.
  mkdir -p $SRC/sites/default/files/images
  mkdir -p $SRC/sites/default/files/xmlsitemap
  mkdir -p $SRC/sites/default/files/private
  ok
}

# Add system vhost
add_vhost() {
  info "Create Virtualhost..."
  if [ -f $VHOST_DIR/$DIR.conf ]; then
    rm -f $VHOST_DIR/$DIR.conf
    rm -f $VHOST_EN_DIR/$DIR.conf
  fi
cat > /tmp/$DIR.tmp << EOF
<VirtualHost *:80>
	ServerAdmin $MAIL
	DocumentRoot $SRC
  ServerName ${DIR}.dvm
	<Directory "$SRC">
		Options FollowSymLinks Indexes
		AllowOverride All
		Order deny,allow
		Allow from all
	</Directory>
  LogLevel warn
	#ErrorLog /var/log/httpd/${DIR}_error.log
  #CustomLog /var/log/httpd/${DIR}_access.log combined
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF

  /usr/bin/mv /tmp/$DIR.tmp $VHOST_DIR/$DIR.conf
  ln -s $VHOST_DIR/$DIR.conf $VHOST_EN_DIR

  ok
  IP=`ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'`
  notice_forced ">> You probably need to add virtualhost information on your client:"
  notice_forced "${IP} ${URL}"
}

# Fix Drupal permissions
# Based from script found at: https://drupal.org/node/244924
fix_permissions() {
  info "Fix permissions..."
  # Changing ownership of all contents
  chown -R ${OWNER}:${GROUP} $SRC
  # Changing permissions of all directories
  find $SRC -type d -exec chmod u=rwx,g=rx,o= '{}' \;
  # Changing permissions of all files
  find $SRC -type f -exec chmod u=rw,g=r,o= '{}' \;
  # Changing permissions of "files"
  find $SRC/sites -type d -name files -exec chmod ug=rwx,o= '{}' \;
  # Changing permissions inside "files"
  for x in $SRC/sites/default/files; do
    find ${x} -type d -exec chmod ug=rwx,o= '{}' \;
    find ${x} -type f -exec chmod ug=rw,o= '{}' \;
  done
  # Git
  if [ -d "$SRC/.git" ]; then
    chmod -R u=rwx,go= $SRC/.git
  fi
  chmod u=rwx,go= $SRC/.gitignore
  # Text files
  chmod u=rwx,go= $SRC/CHANGELOG.txt
  chmod u=rwx,go= $SRC/COPYRIGHT.txt
  chmod u=rwx,go= $SRC/INSTALL.mysql.txt
  chmod u=rwx,go= $SRC/INSTALL.pgsql.txt
  chmod u=rwx,go= $SRC/INSTALL.txt
  chmod u=rwx,go= $SRC/LICENSE.txt
  chmod u=rwx,go= $SRC/MAINTAINERS.txt
  chmod u=rwx,go= $SRC/UPGRADE.txt
  ok
}

drupal_post_install() {
  info "Post install..."
  drush_cmd -v core-cron
  drush_cmd cc all
  drush_cmd bb
  # Add domain url.
  sed -i -e 's/# $base_url.*/$base_url = "http:\/\/'$URL'"; \/\/ No trailing slash!/g' $SRC/sites/default/settings.php
  # Remove http fail warning.
  echo '$conf["drupal_http_request_fails"] = FALSE;' >> $SRC/sites/default/settings.php
  ok
}

deploy() {
  script_infos
  START_TIME=`date -d now +%s`
  START_TIME_MSG=`date -d now +%Hh:%Mmin:%Ssec`
  START_MSG="Starts execution at: ${START_TIME_MSG}"
  notice "$START_MSG"
  if [[ $quiet == 1 ]]; then
    echo "$START_MSG" >> $LOG_FILE
  fi
  # Process all steps
  if [[ $noremove == 0 ]]; then
    remove
  fi
  # Main setup script
  drupal_install
  if [[ $novhost == 0 ]]; then
    add_vhost
  fi
  # Post install
  fix_permissions
  drupal_post_install
  # Print only drush errors or warning.
  if [[ $quiet == 1 ]]; then
    output=$(sed -n '/warning\|error/p' ${LOG_FILE})
  fi
  # Print the runtime
  END_TIME=`date -d now +%s`
  END_TIME_MSG=`date -d now +%Hh:%Mmin:%Ssec`
  RUNTIME=$((END_TIME-START_TIME))
  MINUTES=$((RUNTIME / 60))
  SECONDS=$((RUNTIME % 60))
  END_MSG="Finished execution at: ${END_TIME_MSG}"
  notice "$END_MSG"
  END_SCRTIPT_MSG="Runtime: ${MINUTES}min${SECONDS}sec"
  info "$END_SCRTIPT_MSG"
  if [[ $quiet == 1 ]]; then
    echo "$END_TIME_MSG" >> $LOG_FILE
    echo "$END_SCRTIPT_MSG" >> $LOG_FILE
  fi
}

# Launch main function of this script.
deploy
service httpd restart
